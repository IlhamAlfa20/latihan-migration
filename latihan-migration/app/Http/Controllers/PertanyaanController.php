<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create() {
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        $request->validate([
            'judul' => 'required|unique:pertanyaan',
            'isi' => 'required',
        ]);

        Pertanyaan::ceate([
            'judul' => $request->judul,
            'isi' => $request->isi
        ]);

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil Dibuat');
    }

    public function index() {
        $getPertanyaan = Pertanyaan::all();

        return view('pertanyaan.index', compact('getPertanyaan'));
    }

    public function show($id) {
        $showPertanyaan = Pertanyaan::find($id);

        return view('pertanyaan.show', compact('showPertanyaan'));
    }

    public function edit($id) {
        $pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
        return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request) {
        $request->validate([
            'judul' => 'required',
            'isi' => 'required',
        ]);

        $updatePertanyaan = Pertanyaan::find($id);
        $updatePertanyaan->judul = $request->judul;
        $updatePertanyaan->isi = $request->isi;
        $updatePertanyaan->update();

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil diedit');
    }

    public function destroy($id) {
        $deletePertanyaan = Pertanyaan::find($id);
        $deletePertanyaan->delete();

        return redirect('/pertanyaan')->with('success', 'Pertanyaan Berhasil dihapus');
    }
}
