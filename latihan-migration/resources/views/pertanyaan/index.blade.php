@extends('master')

@section('title')
Lihat Pertanyaan
@endsection

@section('content')
<div class="ml-3 mr-3 mt-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Daftar Pertanyaan</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <a class="btn btn-primary" href="/pertanyaan/create">Buat Pertanyaan</a>
            <table class="table table-bordered mt-3">
                <thead>
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Judul</th>
                        <th>Isi</th>
                        <th style="width: 40px">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($getPertanyaan as $key => $pertanyaan)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $pertanyaan->judul }}</td>
                            <td>{{ $pertanyaan->isi }}</td>
                            <td style="display: flex">
                                <a href="/pertanyaan/{{$pertanyaan->id}}" class="btn btn-info btn-sm">Show</a>
                                <a href="/pertanyaan/{{$pertanyaan->id}}/edit" class="btn btn-default btn-sm">Edit</a>
                                <form action="/pertanyaan/{{$pertanyaan->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center">Tidak Ada Pertanyaan</td>
                        </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
