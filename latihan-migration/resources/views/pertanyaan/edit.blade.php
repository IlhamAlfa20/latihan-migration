@extends('master')

@section('title')
    Edit Pertanyaan
@endsection

@section('content')
<div class="card card-primary ml-3 mt-3 mr-3">
    <div class="card-header">
        <h3 class="card-title">Edit Pertanyaan {{ $pertanyaan->id }}</h3>
    </div>
    <form role="form" action="/pertanyaan/{{ $pertanyaan->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="card-body">
            <div class="form-group">
                <label for="judul">Judul</label>
                <input type="text" class="form-control" id="Judul" name="judul" value="{{ old('judul', $pertanyaan->judul) }}" placeholder="Masukkan Judul Pertanyaan" required>
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="isi">Isi</label>
                <input type="text" class="form-control" id="isi" name="isi" value="{{old('isi', $pertanyaan->isi) }}" placeholder="Masukkan Pertanyaan" required>
                @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
        </div>

        <div class="card-footer">
            <button type="submit" class="btn btn-primary">Edit Pertanyaan</button>
        </div>
    </form>
</div>
@endsection