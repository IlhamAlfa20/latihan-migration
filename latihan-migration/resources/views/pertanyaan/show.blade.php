@extends('master')

@section('title')
    Detail Pertanyaan
@endsection

@section('content')
    <div class="mt-3 ml-3 mr-3">
        <h4>{{ $showPertanyaan->judul }}</h4>
        <p>{{ $showPertanyaan->isi }}</p>
    </div>
@endsection